/**
 * Log functions
 */
module.exports = (function(){
	'use strict';

	var chalk = require('chalk'),
		hasJumpedLine = false,
		indent = ' ';

	var argsToString = function(arg) {
		if (Array.isArray(arg)) {
			return arg.join(',');
		}
		else if (typeof arg === 'object') {
			return JSON.stringify(arg);
		}
		else {
			return typeof arg === 'undefined' ? '' : arg;
		}
	};

	/*
	 * Public functions
	 */

	var trace = function(str) {
		console.log(indent + indent + chalk.gray(argsToString(str)));
		hasJumpedLine = false;
	};

	var debug = function(str) {
		console.log(indent + indent, argsToString(str));
		hasJumpedLine = false;
	};

	var info = function(str) {
		console.log((hasJumpedLine ? '' : '\n') + indent + chalk.cyan.bold(argsToString(str)) + '\n');
		hasJumpedLine = true;
	};

	var warn = function(str) {
		console.warn(indent + indent + chalk.yellow(argsToString(str)));
		hasJumpedLine = false;
	};

	var error = function(str) {
		console.error((hasJumpedLine ? '' : '\n') + indent + chalk.red(argsToString(str)) + '\n');
		hasJumpedLine = true;
	};

	return {
		trace: trace,
		debug: debug,
		info: info,
		warn: warn,
		error: error,
	};
})();
