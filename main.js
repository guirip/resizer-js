#!/usr/bin/env node

/*
	@see: https://github.com/aheckmann/gm

	---
	To use with graphicsmagick (linux command):
		sudo apt-get install graphicsmagick

	---
	To use with imagemagick (linux command) :
		sudo apt-get install imagemagick

	and update code as following :
		gm = require('gm').subClass({imageMagick: true});

	---
	Linux/Mac alias to execute it from anywhere :
		alias resize='node /opt/resizer-js/main.js $@'
 */

(function() {
	'use strict';

	var TYPES = {
			string : typeof '',
			number : typeof 1,
			boolean: typeof true,
		},
		OPTS_DESC = {
			destDir   : {
				type : TYPES.string,
				defaultValue: 'select',
			},
			quality   : {
				type : TYPES.number,
				defaultValue: 94,
			},
			resolution: {
				type : TYPES.number,
				defaultValue: 96,
			},
			size      : {
				type : TYPES.number,
				defaultValue: 1600,
			},
			useStdin  : {
				type : TYPES.boolean,
				defaultValue: false,
			},
		},
		options = {
			destDir   : OPTS_DESC.destDir.defaultValue,
			quality   : OPTS_DESC.quality.defaultValue,
			resolution: OPTS_DESC.resolution.defaultValue,
			size      : OPTS_DESC.size.defaultValue,
			useStdin  : OPTS_DESC.useStdin.defaultValue,
		},
		helpMaxPadding = '                               ';

	var fs = require('fs'),
		gm = require('gm'),
		chalk = require('chalk'),
		Log = require('./Log'),

		cliArgs,
		images = [],
		extensionFilter = /\.([pP][nN][gG]|[jJ][pP][gG]|[jJ][pP][eE][gG]|[bB][mM][pP])$/,
		optionsRegexp = /^--([a-zA-Z]*)=?(.*)$/,
		destinations = [],
		isSimulation = false;


	if (isSimulation === true) {
		Log.info('Mode simulation !');
	}

	if (process.argv.length > 2) {
		cliArgs = process.argv.slice(2);
	}
	else if (options.useStdin !== true) {
		cliArgs = [ process.cwd() ];
	}

	/**
	 * Display usage of this utility
	 */
	var printHelp = function() {
		Log.debug();
		Log.debug('Managed file extensions are (case being insensitive): ' + chalk.bold('png, jpg, jpeg, bmp'));

		Log.debug('Simply type \'' + chalk.cyan('resize') + '\' command to start resizing all the images found in the current directory using the default options.');

		Log.debug();
		Log.debug(chalk.bold('Important notice:') + ' The source pictures are never overwritten! The resized pictures are new files created in a sub-directory.');

		Log.info(' Options');
		Log.debug('These are the available options and their default value:');
		for (var opt in OPTS_DESC) {
			if (OPTS_DESC.hasOwnProperty(opt) === false) {
				continue;
			}
			Log.debug(('  --' + chalk.bold(opt) + '=' + OPTS_DESC[opt].defaultValue + helpMaxPadding).substring(0, helpMaxPadding.length) + chalk.dim('('+OPTS_DESC[opt].type+')'));
		}

		Log.info(' Examples');
		Log.debug('resize --destDir=thumbs --quality=85 --resolution=72 --size=300');
		Log.debug('   This command will:');
		Log.debug('     - resize all images from current directory');
		Log.debug('     - the resized images will be created in a sub-directory called \'thumbs\'');
		Log.debug('     - jpg quality: 85');
		Log.debug('     - ppi: 72');
		Log.debug('     - maximum height or width: 300 pixels');
		Log.debug();
		Log.debug('resize IMG_45896.jpg');
		Log.debug('   This command will:');
		Log.debug('     - resize the file IMG_45896.jpg only');
		Log.debug('     - default options will be used:');
		Log.debug('         - jpg quality: ' + OPTS_DESC.quality.defaultValue);
		Log.debug('         - ppi: ' + OPTS_DESC.resolution.defaultValue);
		Log.debug('         - maximum height or width: ' + OPTS_DESC.size.defaultValue + ' pixels');
		Log.debug();
	};


	var processArgs = function(array) {

		// List all images to resize
		for (var j=0; j<array.length; j++) {
			processArg(array[j]);
		}
	};


	/**
	 * find images to resize
	 * @param {string} source
	 */
	var processArg = function(source) {
		if (typeof source === 'undefined' || source.trim() === '') {
			return;
		}

		var optionMatched = optionsRegexp.exec(source);
		if (optionMatched && optionMatched.length) {
			var name = optionMatched[1],
				value = optionMatched[2];

			if (name === 'help') {
				printHelp();
				process.exit(0);
			}
			else if (options[name] === undefined) {
				Log.error('unknown option ' + name);
			}
			else {
				switch (OPTS_DESC[name].type) {
					case TYPES.string:
						options[name] = value;
						break;
					case TYPES.number:
						options[name] = parseInt(value, 10);
						break;
					case TYPES.boolean:
						options[name] = value === 'true' || value === '1' ? true : false;
						break;
					default:
						Log.error('unhandled option type: ' + OPTS_DESC[name].type);

				}
				Log.info(chalk.blue(name) + ' option: ' + chalk.green(options[name]));
			}
			return;
		}

		var sourceStats;
		try {
			sourceStats = fs.lstatSync(source);
		} catch (e) {
			Log.error(source + ' does not exist');
		}

		if (sourceStats) {
			if (sourceStats.isFile()) {
				if (extensionFilter.test(source) === false) {
					Log.trace('ignore ' + source);
				} else {
					images.push(source);
				}
			}
			else if (sourceStats.isDirectory()) {
				if (source.indexOf('/' + options.destDir) !== -1){
					Log.debug('"' + source.replace(options.destDir, chalk.underline(options.destDir)) +
						'" looks more like a resize destination than a source directory, so ignoring it.');
				} else {
					var directoryContent = fs.readdirSync(source);
					for (var i=0; i<directoryContent.length; i++) {
						processArg(source + (source.endsWith('/') ? '' : '/') + directoryContent[i]);
					}
				}
			}
		}
	};

	/**
	 * Check that destination directory exists, create it if needed
	 * @param  {string} path
	 * @return {string} full destination path
	 */
	var checkDestination = function(path) {
		if (!path) {
			Log.error('undefined path value');
			return;
		}
		var lastSlashIndex = path.lastIndexOf('/'),
			destDirPath = path.substring(0, lastSlashIndex+1) + options.destDir;

		// Avoid handling many times the same destination path
		if (!destinations[destDirPath]) {
			try {
				fs.lstatSync(destDirPath);
			} catch (e) {
				// Create destination directory
				fs.mkdirSync(destDirPath);
			}
			destinations[destDirPath] = true;
		}
		return destDirPath + '/' + path.substring(lastSlashIndex);
	};


	/**
	 * Start iteration on all images to resize
	 */
	var start = function() {

		if (images.length === 0) {
			Log.error('No image found');
		} else {
			var count = 0,
				dateStart = new Date().getTime();

			var proceed = function() {
				Log.debug('Resizing ' + images[count]);

				if (isSimulation === true) {
					next();
				} else {
					gm(images[count]).size(performResize);
				}
			};

			var performResize = function(err, size){
				if (err) {
					Log.error('Could not get the size of ' + this.source + ' because: ' + err);
					next();
					return;
				}
				var isPortrait = size.height > size.width;
				var destination = checkDestination(this.source);

				this.resize(isPortrait ? null : options.size, isPortrait ? options.size : null)
					.noProfile()
					.autoOrient()
					//.resample(options.resolution, options.resolution)
					.quality(options.quality)
					.write(destination, writeEndHandler);
			};

			var writeEndHandler = function(err) {
				next();
				if (err) {
					Log.error('Could not perform resize because: ' + err);
					return;
				}
			};

			var next = function() {
				count++;
				if (count === images.length) {
					summary();
					process.exit(0);
				} else {
					proceed();
				}
			};

			var summary = function() {
				var nbMsInAMinute = 60 * 1000,
					duration = new Date().getTime() - dateStart,
					nbMin = Math.floor(duration / nbMsInAMinute),
					nbSec = Math.floor((duration % nbMsInAMinute) / 1000);

				Log.info(chalk.dim('Duration: ' + (nbMin > 0 ? nbMin + 'min ' : '') + (nbSec+1) + 's'));
			};

			Log.info('Proceeding...');
			proceed();
		}
	};


	// Parse options and optional paths
	processArgs(cliArgs);


	if (options.useStdin !== true) {
		// Regular execution
		start();
	}
	else {
		// Handle args from pipe (ex: cat pictures-list.txt | resize --useStdin=true --size=1600)
		process.stdin.setEncoding('utf8');
		process.stdin.on('data', function(chunk) {
			processArgs(chunk.split('\n'));
			start();
		});
	}

})();